<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="./assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>LUXDRAWS</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
        <!-- CSS Files -->
        <!-- <link href="./assets/css/bootstrap.min.css" rel="stylesheet" /> -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="./assets/css/style.css?v=1.1.0" rel="stylesheet" />

         <link href="./assets/css/sudheer-global.css?v=1.1.0" rel="stylesheet" />

         <link href="./assets/css/themify-icons.css?v=1.1.0" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="./assets/css/demo.css" rel="stylesheet" />
    </head>
    <body class="landing-page sidebar-collapse">
        <?php
            $current = 'dashboard'
        ?>
        <?php
        include("_inc/nav.php");
        ?>
        <!-- End Navbar -->
        <div class="wrapper">
            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-sm">
                            <div class="total-products card" data-background-color="red">
                                <div class="global-padding pt-4plus pb-4">
                                    Total Products
                                    <div class="total-count">
                                        222
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="active-products card" data-background-color="green">
                                <div class="global-padding pt-4plus pb-4">
                                    Active Products
                                    <div class="total-count">
                                        123
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="total-customers card" data-background-color="blue">
                                <div class="global-padding pt-4plus pb-4">
                                    Total Customers
                                    <div class="total-count">
                                        1,23,456
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-12 title-dotted-style space-1">
                            <h4>Active Products</h4>
                        </div>
                        <div class="col-12">
                            <div class="bg-white-bordered global-padding-more space-2">
                                <div class="product-table space-1 hide-till-sm">
                                    <div class="product-table-title">
                                        <div class="product-table-title-p-info">
                                            Product Info
                                        </div>
                                        <div class="product-table-title-start">
                                            Start Date
                                        </div>
                                        <div class="product-table-title-end">
                                            End date
                                        </div>
                                        <div class="product-table-title-value">
                                            Value
                                        </div>
                                        <div class="product-table-title-status">
                                            Status
                                        </div>
                                        <div class="product-table-title-action text-center">
                                            Action
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row space-1">
                                    <div class="bg-white-bordered col-12">
                                        <div class="product-table py-2">
                                            <div class="product-table-title">
                                                <div class="product-table-title-p-info">

                                                    <div class="media">
                                                        <img class="align-self-center mr-3" src="./assets/img/product_thumbnail.png" alt="Generic placeholder image">

                                                        <div class="media-body">

                                                            <div class="extra-style">

                                                                <div class="mt-0 p-info-title">Name:</div>
                                                                <div class="p-info-desc">Rolex Watch</div>

                                                            </div>

                                                            <div class="">

                                                                <div class="mt-0 p-info-title">Model:</div>
                                                                <div class="p-info-desc">UNM123E</div>

                                                            </div>

                                                            <div class="mobile-active">
                                                                Active
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="product-table-title-start">

                                                    <div class="product-table-title-mobile">
                                                        Start Date
                                                    </div>

                                                    <div class="mt-0 p-info-title">
                                                        <div class="hide-till-sm">Date:</div>
                                                    </div>
                                                    <div class="p-info-desc">
                                                        <span class="color-blue">
                                                            10/01/2018
                                                        </span>
                                                    </div>

                                                </div>

                                                <div class="product-table-title-end">

                                                    <div class="product-table-title-mobile">
                                                        End Date
                                                    </div>

                                                    <div class="mt-0 p-info-title">
                                                        <div class="hide-till-sm">Date:</div>
                                                    </div>
                                                    <div class="p-info-desc secondary-color">
                                                        <span class="color-pink">
                                                            10/04/2018
                                                        </span>
                                                    </div>

                                                </div>

                                                <div class="product-table-title-value">

                                                    <div class="product-table-title-mobile">
                                                        Value
                                                    </div>

                                                    <div class="mt-0 p-info-title">
                                                        <div class="hide-till-sm">Ticket Price:</div>
                                                        
                                                    </div>
                                                    <div class="p-info-desc">
                                                        <span class="color-orange">
                                                            $ 200
                                                        </span>
                                                    </div>

                                                </div>

                                                <div class="product-table-title-status">
                                                    <div class="success-color">
                                                        Active
                                                    </div>
                                                </div>
                                                <div class="product-table-title-action text-center">
                                                    <a href="#" class="clearfix">
                                                        <div class="edit-btn">
                                                            <span class="ti-pencil"></span>
                                                        </div>
                                                        <div class="delete-btn">
                                                            <span class="ti-close"></span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row space-1">
                                    <div class="bg-white-bordered col-12">
                                        <div class="product-table py-2">
                                            <div class="product-table-title">
                                                <div class="product-table-title-p-info">

                                                    <div class="media">
                                                        <img class="align-self-center mr-3" src="./assets/img/product_thumbnail.png" alt="Generic placeholder image">

                                                        <div class="media-body">

                                                            <div class="extra-style">

                                                                <div class="mt-0 p-info-title">Name:</div>
                                                                <div class="p-info-desc">Rolex Watch</div>

                                                            </div>

                                                            <div class="">

                                                                <div class="mt-0 p-info-title">Model:</div>
                                                                <div class="p-info-desc">UNM123E</div>

                                                            </div>

                                                            <div class="mobile-active">
                                                                Active
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="product-table-title-start">

                                                    <div class="product-table-title-mobile">
                                                        Start Date
                                                    </div>

                                                    <div class="mt-0 p-info-title">
                                                        <div class="hide-till-sm">Date:</div>
                                                    </div>
                                                    <div class="p-info-desc">
                                                        <span class="color-blue">
                                                            10/01/2018
                                                        </span>
                                                    </div>

                                                </div>

                                                <div class="product-table-title-end">

                                                    <div class="product-table-title-mobile">
                                                        End Date
                                                    </div>

                                                    <div class="mt-0 p-info-title">
                                                        <div class="hide-till-sm">Date:</div>
                                                    </div>
                                                    <div class="p-info-desc secondary-color">
                                                        <span class="color-pink">
                                                            10/04/2018
                                                        </span>
                                                    </div>

                                                </div>

                                                <div class="product-table-title-value">

                                                    <div class="product-table-title-mobile">
                                                        Value
                                                    </div>

                                                    <div class="mt-0 p-info-title">
                                                        <div class="hide-till-sm">Ticket Price:</div>
                                                        
                                                    </div>
                                                    <div class="p-info-desc">
                                                        <span class="color-orange">
                                                            $ 200
                                                        </span>
                                                    </div>

                                                </div>

                                                <div class="product-table-title-status">
                                                    <div class="success-color">
                                                        Active
                                                    </div>
                                                </div>
                                                <div class="product-table-title-action text-center">
                                                    <a href="#" class="clearfix">
                                                        <div class="edit-btn">
                                                            <span class="ti-pencil"></span>
                                                        </div>
                                                        <div class="delete-btn">
                                                            <span class="ti-close"></span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row space-1">
                                    <div class="bg-white-bordered col-12">
                                        <div class="product-table py-2">
                                            <div class="product-table-title">
                                                <div class="product-table-title-p-info">

                                                    <div class="media">
                                                        <img class="align-self-center mr-3" src="./assets/img/product_thumbnail.png" alt="Generic placeholder image">

                                                        <div class="media-body">

                                                            <div class="extra-style">

                                                                <div class="mt-0 p-info-title">Name:</div>
                                                                <div class="p-info-desc">Rolex Watch</div>

                                                            </div>

                                                            <div class="">

                                                                <div class="mt-0 p-info-title">Model:</div>
                                                                <div class="p-info-desc">UNM123E</div>

                                                            </div>

                                                            <div class="mobile-active">
                                                                Active
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="product-table-title-start">

                                                    <div class="product-table-title-mobile">
                                                        Start Date
                                                    </div>

                                                    <div class="mt-0 p-info-title">
                                                        <div class="hide-till-sm">Date:</div>
                                                    </div>
                                                    <div class="p-info-desc">
                                                        <span class="color-blue">
                                                            10/01/2018
                                                        </span>
                                                    </div>

                                                </div>

                                                <div class="product-table-title-end">

                                                    <div class="product-table-title-mobile">
                                                        End Date
                                                    </div>

                                                    <div class="mt-0 p-info-title">
                                                        <div class="hide-till-sm">Date:</div>
                                                    </div>
                                                    <div class="p-info-desc secondary-color">
                                                        <span class="color-pink">
                                                            10/04/2018
                                                        </span>
                                                    </div>

                                                </div>

                                                <div class="product-table-title-value">

                                                    <div class="product-table-title-mobile">
                                                        Value
                                                    </div>

                                                    <div class="mt-0 p-info-title">
                                                        <div class="hide-till-sm">Ticket Price:</div>
                                                        
                                                    </div>
                                                    <div class="p-info-desc">
                                                        <span class="color-orange">
                                                            $ 200
                                                        </span>
                                                    </div>

                                                </div>

                                                <div class="product-table-title-status">
                                                    <div class="success-color">
                                                        Active
                                                    </div>
                                                </div>
                                                <div class="product-table-title-action text-center">
                                                    <a href="#" class="clearfix">
                                                        <div class="edit-btn">
                                                            <span class="ti-pencil"></span>
                                                        </div>
                                                        <div class="delete-btn">
                                                            <span class="ti-close"></span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row space-1">
                                    <div class="bg-white-bordered col-12">
                                        <div class="product-table py-2">
                                            <div class="product-table-title">
                                                <div class="product-table-title-p-info">

                                                    <div class="media">
                                                        <img class="align-self-center mr-3" src="./assets/img/product_thumbnail.png" alt="Generic placeholder image">

                                                        <div class="media-body">

                                                            <div class="extra-style">

                                                                <div class="mt-0 p-info-title">Name:</div>
                                                                <div class="p-info-desc">Rolex Watch</div>

                                                            </div>

                                                            <div class="">

                                                                <div class="mt-0 p-info-title">Model:</div>
                                                                <div class="p-info-desc">UNM123E</div>

                                                            </div>

                                                            <div class="mobile-active">
                                                                Active
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="product-table-title-start">

                                                    <div class="product-table-title-mobile">
                                                        Start Date
                                                    </div>

                                                    <div class="mt-0 p-info-title">
                                                        <div class="hide-till-sm">Date:</div>
                                                    </div>
                                                    <div class="p-info-desc">
                                                        <span class="color-blue">
                                                            10/01/2018
                                                        </span>
                                                    </div>

                                                </div>

                                                <div class="product-table-title-end">

                                                    <div class="product-table-title-mobile">
                                                        End Date
                                                    </div>

                                                    <div class="mt-0 p-info-title">
                                                        <div class="hide-till-sm">Date:</div>
                                                    </div>
                                                    <div class="p-info-desc secondary-color">
                                                        <span class="color-pink">
                                                            10/04/2018
                                                        </span>
                                                    </div>

                                                </div>

                                                <div class="product-table-title-value">

                                                    <div class="product-table-title-mobile">
                                                        Value
                                                    </div>

                                                    <div class="mt-0 p-info-title">
                                                        <div class="hide-till-sm">Ticket Price:</div>
                                                        
                                                    </div>
                                                    <div class="p-info-desc">
                                                        <span class="color-orange">
                                                            $ 200
                                                        </span>
                                                    </div>

                                                </div>

                                                <div class="product-table-title-status">
                                                    <div class="success-color">
                                                        Active
                                                    </div>
                                                </div>
                                                <div class="product-table-title-action text-center">
                                                    <a href="#" class="clearfix">
                                                        <div class="edit-btn">
                                                            <span class="ti-pencil"></span>
                                                        </div>
                                                        <div class="delete-btn">
                                                            <span class="ti-close"></span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <?php
        include("_inc/footer.php");
        ?>