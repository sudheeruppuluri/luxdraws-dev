<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="./assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>LUXDRAWS</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
        <!-- CSS Files -->
        <!-- <link href="./assets/css/bootstrap.min.css" rel="stylesheet" /> -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="./assets/css/style.css?v=1.1.0" rel="stylesheet" />

    </head>
    <body class="landing-page sidebar-collapse">
        <?php
        include("_inc/nav.php");
        ?>
        <!-- End Navbar -->
        <div class="wrapper" style="padding-top: 150px; margin-bottom: 1900px;">
            

            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-12 title-dotted-style space-1">
                            <h4>Products</h4>
                        </div>
                        <div class="col-12">
                            <div class="bg-white-bordered global-padding-more space-2">
                                <div class="product-table space-1">
                                    <div class="product-table-title">
                                        <div class="product-table-title-p-info">
                                            Product Info
                                        </div>
                                        <div class="product-table-title-d-info">
                                            Date Info
                                        </div>
                                        <div class="product-table-title-price">
                                            Price
                                        </div>
                                        <div class="product-table-title-uname">
                                            User Name
                                        </div>
                                        <div class="product-table-title-purchased">
                                            Purchased
                                        </div>
                                        <div class="product-table-title-status">
                                            Status
                                        </div>
                                        <div class="product-table-title-edit">
                                            Edit
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="bg-white-bordered col-12">
                                        <div class="product-table py-2">
                                            <div class="product-table-title">
                                                <div class="product-table-title-p-info">
                                                    <div class="media">
                                                        <img class="align-self-center mr-3" src="./assets/img/product_thumbnail.png" alt="Generic placeholder image">
                                                        <div class="media-body">
                                                            <div class="mt-0">Name</div>
                                                            <small>Rolex Watch</small>
                                                            <div class="mt-0">Model</div>
                                                            <small>UNM213RT</small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="product-table-title-d-info">
                                                    Date Info
                                                </div>
                                                <div class="product-table-title-price">
                                                    Price
                                                </div>
                                                <div class="product-table-title-uname">
                                                    User Name
                                                </div>
                                                <div class="product-table-title-purchased">
                                                    Purchased
                                                </div>
                                                <div class="product-table-title-status">
                                                    Status
                                                </div>
                                                <div class="product-table-title-edit">
                                                    Edit
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <?php
        include("_inc/footer.php");
        ?>