<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="./assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>LUXDRAWS</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
        <!-- CSS Files -->
        <link href="./assets/css/bootstrap.min.css" rel="stylesheet" />
        
        <link href="./assets/css/style.css?v=1.1.0" rel="stylesheet" />
        <link href="./assets/css/sudheer-global.css?v=1.1.0" rel="stylesheet" />
        <link href="./assets/css/add_product.css?v=1.1.0" rel="stylesheet" />
        <link href="./assets/css/themify-icons.css?v=1.1.0" rel="stylesheet" />
        
    </head>
    <body class="landing-page sidebar-collapse">
        <?php
        $current = 'product-list'
        ?>
        <?php
        include("_inc/nav.php");
        ?>
        <!-- End Navbar -->
        <div class="wrapper" style="padding-top: 150px;">
            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-12 title-dotted-style space-1">
                            <h4>Product List</h4>
                        </div>
                        <div class="col-12">
                            <div class="bg-white-bordered global-padding-more space-2">
                                <div class="mt-3" style="border-bottom: 1px solid #ccc;">
                                    <div class="form-group row space-3">
                                        <div class="col-sm-3"></div>
                                        
                                        <div class="col-sm-4 space-1">
                                            <input type="text" class="form-control" placeholder="search" data-search>
                                        </div>
                                        <div class="col-sm-2">
                                            <button class="btn btn-primary m-auto">
                                            Search
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-table space-1 hide-till-sm mt-5" style="">
                                    <div class="product-table-title">
                                        <div class="product-table-title-p-info">
                                            Product Info
                                        </div>
                                        <div class="product-table-title-start">
                                            Start Date
                                        </div>
                                        <div class="product-table-title-end">
                                            End date
                                        </div>
                                        <div class="product-table-title-value">
                                            Value
                                        </div>
                                        <div class="product-table-title-status">
                                            Status
                                        </div>
                                        <div class="product-table-title-action text-center">
                                            Action
                                        </div>
                                    </div>
                                </div>
                                <div class="grid">
                                    <div class="row space-2 grid-item" data-filter-item data-filter-name="rolex">
                                        <div class="bg-white-bordered col-12">
                                            <div class="product-table py-2">
                                                <div class="product-table-title">
                                                    <div class="product-table-title-p-info">
                                                        <div class="media">
                                                            <img class="align-self-center mr-3 product-table-image" src="./assets/img/rolex.jpg" alt="Generic placeholder image">
                                                            <div class="media-body">
                                                                <div class="extra-style">
                                                                    <div class="mt-0 p-info-title">Name:</div>
                                                                    <div class="p-info-desc">Rolex</div>
                                                                </div>
                                                                <div class="">
                                                                    <div class="mt-0 p-info-title">Model:</div>
                                                                    <div class="p-info-desc">UNM123E</div>
                                                                </div>
                                                                <div class="mobile-active">
                                                                    Active
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-start">
                                                        <div class="product-table-title-mobile">
                                                            Start Date
                                                        </div>
                                                        <div class="mt-0 p-info-title">
                                                            <div class="hide-till-sm">Date:</div>
                                                        </div>
                                                        <div class="p-info-desc">
                                                            <span class="color-blue">
                                                                10/01/2018
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-end">
                                                        <div class="product-table-title-mobile">
                                                            End Date
                                                        </div>
                                                        <div class="mt-0 p-info-title">
                                                            <div class="hide-till-sm">Date:</div>
                                                        </div>
                                                        <div class="p-info-desc secondary-color">
                                                            <span class="color-pink">
                                                                10/04/2018
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-value">
                                                        <div class="product-table-title-mobile">
                                                            Value
                                                        </div>
                                                        <div class="mt-0 p-info-title">
                                                            <div class="hide-till-sm">Ticket Price:</div>
                                                            
                                                        </div>
                                                        <div class="p-info-desc">
                                                            <span class="color-orange">
                                                                $ 200
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-status">
                                                        <div class="success-color">
                                                            Active
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-action text-center">
                                                        <div class="clearfix">
                                                            <a href="editproduct.php" class="edit-btn">
                                                                <span class="ti-pencil"></span>
                                                            </a>
                                                            <div class="delete-btn" data-toggle="modal" data-target="#myModal1">
                                                                <span class="ti-close"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row space-2 grid-item" data-filter-item data-filter-name="tag heuer">
                                        <div class="bg-white-bordered col-12">
                                            <div class="product-table py-2">
                                                <div class="product-table-title">
                                                    <div class="product-table-title-p-info">
                                                        <div class="media">
                                                            <img class="align-self-center mr-3 product-table-image" src="./assets/img/tag.jpg" alt="Generic placeholder image">
                                                            <div class="media-body">
                                                                <div class="extra-style">
                                                                    <div class="mt-0 p-info-title">Name:</div>
                                                                    <div class="p-info-desc">Tag heuer</div>
                                                                </div>
                                                                <div class="">
                                                                    <div class="mt-0 p-info-title">Model:</div>
                                                                    <div class="p-info-desc">TH145</div>
                                                                </div>
                                                                <div class="mobile-active">
                                                                    Active
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-start">
                                                        <div class="product-table-title-mobile">
                                                            Start Date
                                                        </div>
                                                        <div class="mt-0 p-info-title">
                                                            <div class="hide-till-sm">Date:</div>
                                                        </div>
                                                        <div class="p-info-desc">
                                                            <span class="color-blue">
                                                                09/02/2018
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-end">
                                                        <div class="product-table-title-mobile">
                                                            End Date
                                                        </div>
                                                        <div class="mt-0 p-info-title">
                                                            <div class="hide-till-sm">Date:</div>
                                                        </div>
                                                        <div class="p-info-desc secondary-color">
                                                            <span class="color-pink">
                                                                10/04/2018
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-value">
                                                        <div class="product-table-title-mobile">
                                                            Value
                                                        </div>
                                                        <div class="mt-0 p-info-title">
                                                            <div class="hide-till-sm">Ticket Price:</div>
                                                            
                                                        </div>
                                                        <div class="p-info-desc">
                                                            <span class="color-orange">
                                                                $ 100
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-status">
                                                        <div class="success-color">
                                                            Active
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-action text-center">
                                                        <a href="#" class="clearfix">
                                                            <div class="edit-btn">
                                                                <span class="ti-pencil"></span>
                                                            </div>
                                                            <div class="delete-btn" data-toggle="modal" data-target="#myModal1">
                                                                <span class="ti-close"></span>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row space-2 grid-item" data-filter-item data-filter-name="patek philippe">
                                        <div class="bg-white-bordered col-12">
                                            <div class="product-table py-2">
                                                <div class="product-table-title">
                                                    <div class="product-table-title-p-info">
                                                        <div class="media">
                                                            <img class="align-self-center mr-3 product-table-image" src="./assets/img/patek.jpg" alt="Generic placeholder image">
                                                            <div class="media-body">
                                                                <div class="extra-style">
                                                                    <div class="mt-0 p-info-title">Name:</div>
                                                                    <div class="p-info-desc">Patek Philippe</div>
                                                                </div>
                                                                <div class="">
                                                                    <div class="mt-0 p-info-title">Model:</div>
                                                                    <div class="p-info-desc">PP5676</div>
                                                                </div>
                                                                <div class="mobile-expired">
                                                                    Expired
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-start">
                                                        <div class="product-table-title-mobile">
                                                            Start Date
                                                        </div>
                                                        <div class="mt-0 p-info-title">
                                                            <div class="hide-till-sm">Date:</div>
                                                        </div>
                                                        <div class="p-info-desc">
                                                            <span class="color-blue">
                                                                10/02/2018
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-end">
                                                        <div class="product-table-title-mobile">
                                                            End Date
                                                        </div>
                                                        <div class="mt-0 p-info-title">
                                                            <div class="hide-till-sm">Date:</div>
                                                        </div>
                                                        <div class="p-info-desc secondary-color">
                                                            <span class="color-pink">
                                                                10/03/2018
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-value">
                                                        <div class="product-table-title-mobile">
                                                            Value
                                                        </div>
                                                        <div class="mt-0 p-info-title">
                                                            <div class="hide-till-sm">Ticket Price:</div>
                                                            
                                                        </div>
                                                        <div class="p-info-desc">
                                                            <span class="color-orange">
                                                                $ 130
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-status">
                                                        <div class="expired-color">
                                                            Expired
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-action text-center">
                                                        <a href="#" class="clearfix">
                                                            <div class="edit-btn">
                                                                <span class="ti-pencil"></span>
                                                            </div>
                                                            <div class="delete-btn" data-toggle="modal" data-target="#myModal1">
                                                                <span class="ti-close"></span>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row space-2 grid-item" data-filter-item data-filter-name="vacheron constantin">
                                        <div class="bg-white-bordered col-12">
                                            <div class="product-table py-2">
                                                <div class="product-table-title">
                                                    <div class="product-table-title-p-info">
                                                        <div class="media">
                                                            <img class="align-self-center mr-3 product-table-image" src="./assets/img/vacheron.jpg" alt="Generic placeholder image">
                                                            <div class="media-body">
                                                                <div class="extra-style">
                                                                    <div class="mt-0 p-info-title">Name:</div>
                                                                    <div class="p-info-desc">Vacheron Constantin</div>
                                                                </div>
                                                                <div class="">
                                                                    <div class="mt-0 p-info-title">Model:</div>
                                                                    <div class="p-info-desc">VC9087</div>
                                                                </div>
                                                                <div class="mobile-active">
                                                                    Active
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-start">
                                                        <div class="product-table-title-mobile">
                                                            Start Date
                                                        </div>
                                                        <div class="mt-0 p-info-title">
                                                            <div class="hide-till-sm">Date:</div>
                                                        </div>
                                                        <div class="p-info-desc">
                                                            <span class="color-blue">
                                                                08/01/2018
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-end">
                                                        <div class="product-table-title-mobile">
                                                            End Date
                                                        </div>
                                                        <div class="mt-0 p-info-title">
                                                            <div class="hide-till-sm">Date:</div>
                                                        </div>
                                                        <div class="p-info-desc secondary-color">
                                                            <span class="color-pink">
                                                                07/04/2018
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-value">
                                                        <div class="product-table-title-mobile">
                                                            Value
                                                        </div>
                                                        <div class="mt-0 p-info-title">
                                                            <div class="hide-till-sm">Ticket Price:</div>
                                                            
                                                        </div>
                                                        <div class="p-info-desc">
                                                            <span class="color-orange">
                                                                $ 120
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-status">
                                                        <div class="success-color">
                                                            Active
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-action text-center">
                                                        <a href="#" class="clearfix">
                                                            <div class="edit-btn">
                                                                <span class="ti-pencil"></span>
                                                            </div>
                                                            <div class="delete-btn" data-toggle="modal" data-target="#myModal1">
                                                                <span class="ti-close"></span>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row space-2 grid-item" data-filter-item data-filter-name="audemars piguet">
                                        <div class="bg-white-bordered col-12">
                                            <div class="product-table py-2">
                                                <div class="product-table-title">
                                                    <div class="product-table-title-p-info">
                                                        <div class="media">
                                                            <img class="align-self-center mr-3 product-table-image" src="./assets/img/audemars.jpg" alt="Generic placeholder image">
                                                            <div class="media-body">
                                                                <div class="extra-style">
                                                                    <div class="mt-0 p-info-title">Name:</div>
                                                                    <div class="p-info-desc">Audemars Piguet</div>
                                                                </div>
                                                                <div class="">
                                                                    <div class="mt-0 p-info-title">Model:</div>
                                                                    <div class="p-info-desc">AP7812</div>
                                                                </div>
                                                                <div class="mobile-expired">
                                                                    expired
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-start">
                                                        <div class="product-table-title-mobile">
                                                            Start Date
                                                        </div>
                                                        <div class="mt-0 p-info-title">
                                                            <div class="hide-till-sm">Date:</div>
                                                        </div>
                                                        <div class="p-info-desc">
                                                            <span class="color-blue">
                                                                02/01/2018
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-end">
                                                        <div class="product-table-title-mobile">
                                                            End Date
                                                        </div>
                                                        <div class="mt-0 p-info-title">
                                                            <div class="hide-till-sm">Date:</div>
                                                        </div>
                                                        <div class="p-info-desc secondary-color">
                                                            <span class="color-pink">
                                                                01/03/2018
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-value">
                                                        <div class="product-table-title-mobile">
                                                            Value
                                                        </div>
                                                        <div class="mt-0 p-info-title">
                                                            <div class="hide-till-sm">Ticket Price:</div>
                                                            
                                                        </div>
                                                        <div class="p-info-desc">
                                                            <span class="color-orange">
                                                                $ 50
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-status">
                                                        <div class="expired-color">
                                                            Expired
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-action text-center">
                                                        <a href="#" class="clearfix">
                                                            <div class="edit-btn">
                                                                <span class="ti-pencil"></span>
                                                            </div>
                                                            <div class="delete-btn" data-toggle="modal" data-target="#myModal1">
                                                                <span class="ti-close"></span>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </section>
        </div>

        <!-- Modal Core -->
        <!-- <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title color-orange" id="myModalLabel">Delete for Sure?</h4>
              </div>
              <div class="modal-body">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-info btn-simple popup-delete" data-dismiss="modal" aria-hidden="true">Yes</button>
              </div>
            </div>
          </div>
        </div> -->


         <!-- Mini Modal -->
        <div class="modal fade modal-mini modal-danger" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header justify-content-center">
                        <div class="modal-profile">
                            <i class="ti-close"></i>
                        </div>
                    </div>
                    <div class="modal-body">
                        <h5 class="color-white text-center">Delete for Sure?</h5>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link btn-neutral" data-dismiss="modal">No</button>
                        <button type="button" class="btn btn-link btn-neutral" data-dismiss="modal">Yes</button>
                    </div>
                </div>
            </div>
        </div>
        
        <?php
        include("_inc/footer.php");
        ?>

        <script type="text/javascript">
            $(".delete-btn").on('click', function(){
              $(this).closest('.grid-item').remove();
            });
        </script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script> 

        <script src="./assets/js/new_luxdraws.js" type="text/javascript"></script>