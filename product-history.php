<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="./assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>LUXDRAWS</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
        <!-- CSS Files -->
        <link href="./assets/css/bootstrap.min.css" rel="stylesheet" />
        
        <link href="./assets/css/style.css?v=1.1.0" rel="stylesheet" />
        <link href="./assets/css/sudheer-global.css?v=1.1.0" rel="stylesheet" />
        <link href="./assets/css/add_product.css?v=1.1.0" rel="stylesheet" />
        <link href="./assets/css/themify-icons.css?v=1.1.0" rel="stylesheet" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />
        
        
    </head>
    <body class="landing-page sidebar-collapse">
        <?php
        $current = 'product-history'
        ?>
        <?php
        include("_inc/nav.php");
        ?>
        <!-- End Navbar -->
        <div class="wrapper">
            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-12 title-dotted-style space-1">
                            <h4>Product History</h4>
                        </div>
                        <div class="col-12">
                            <div class="bg-white-bordered space-2">
                                <div class="" style="border-bottom: 1px solid #ccc; background: #f5f5f5;">
                                    <div class="form-group row space-3 slide">
                                        <div class="ui-widget">
                                            <p>Select your product</p>
                                            <input type="text" placeholder="Select" autocomplete="on" />
                                        </div>
                                    </div>
                                </div>

                            <div class="" style="border-bottom: 1px solid #ccc;">
                                <div class="global-padding">
                                    <div class="row grid-item">
                                        <div class="col-12">
                                            <div class="product-table py-2">
                                                <div class="product-table-title">
                                                    <div class="product-table-title-p-info" style="width: 30% !important;">
                                                        <div class="media">
                                                            <img class="align-self-center mr-3 product-table-image" src="./assets/img/rolex.jpg" alt="Generic placeholder image">
                                                            <div class="media-body">
                                                                <div class="extra-style">
                                                                    <div class="mt-0 p-info-title">Name:</div>
                                                                    <div class="p-info-desc">Rolex</div>
                                                                </div>
                                                                <div class="">
                                                                    <div class="mt-0 p-info-title">Model:</div>
                                                                    <div class="p-info-desc">UNM123E</div>
                                                                </div>
                                                                <div class="mobile-active">
                                                                    Active
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-start">
                                                        <div class="product-table-title-mobile">
                                                            Start Date
                                                        </div>
                                                        <div class="mt-0 p-info-title">
                                                            <div class="hide-till-sm">Start Date:</div>
                                                        </div>
                                                        <div class="p-info-desc">
                                                            <span class="color-blue">
                                                                10/01/2018
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-end">
                                                        <div class="product-table-title-mobile">
                                                            End Date
                                                        </div>
                                                        <div class="mt-0 p-info-title">
                                                            <div class="hide-till-sm">End Date:</div>
                                                        </div>
                                                        <div class="p-info-desc secondary-color">
                                                            <span class="color-pink">
                                                                10/04/2018
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-value mobile-border-bottom">
                                                        <div class="product-table-title-mobile">
                                                            Value
                                                        </div>
                                                        <div class="mt-0 p-info-title">
                                                            <div class="hide-till-sm">Ticket Price:</div>
                                                            
                                                        </div>
                                                        <div class="p-info-desc">
                                                            <span class="color-orange">
                                                                $ 200
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-value mobile-border-bottom">
                                                        <div class="product-table-title-mobile">
                                                            Total
                                                        </div>
                                                        <div class="mt-0 p-info-title">
                                                            <div class="hide-till-sm">Total Ticket:</div>
                                                            
                                                        </div>
                                                        <div class="p-info-desc">
                                                            <span class="">
                                                                200
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-value">
                                                        <div class="product-table-title-mobile">
                                                            Sold
                                                        </div>
                                                        <div class="mt-0 p-info-title">
                                                            <div class="hide-till-sm">Sold Ticket:</div>
                                                            
                                                        </div>
                                                        <div class="p-info-desc">
                                                            <span class="">
                                                                120
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="product-table-title-status">
                                                        <div class="mt-0 p-info-title">
                                                            <div class="hide-till-sm">Status:</div>
                                                            
                                                        </div>
                                                        <div class="p-info-desc">
                                                            <div class="success-color">
                                                                Active
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="global-padding mt-3" style="min-height: 500px;">
                                 <div class="row">
                                    <form class="form-inline col-12 mb-2">
                                        <div class="form-group space-3 ml-md-auto w-xs-100">
                                            <!-- <div class="col-sm-6"></div> -->
                                            <!-- <div class="col-md-4 offset-md-4 space-1">
                                                <input type="text" class="form-control" placeholder="search" data-search>
                                            </div> -->
                                            <label for="inputPassword2" class="sr-only">    Search
                                            </label>
                                            <input type="text" class="form-control xs-block w-300" id="" placeholder="Search">
                                            <button type="submit" class="btn btn-primary ml-2 ml-xs-none">Search</button>
                                            <!-- <div class="col-sm-2">
                                                <button class="btn btn-primary m-auto">
                                                    Search
                                                </button>
                                            </div> -->
                                        </div>
                                    </form>
                                </div>

                                <div class="col-12">
                                    <div class="history space-1 hide-till-sm mt-5" style="">
                                        <div class="history-table">
                                            <div class="history-table-date">
                                                Date & Time
                                            </div>
                                            <div class="history-table-user">
                                                User Name
                                            </div>
                                            <div class="history-table-purchased d-t-c">
                                                Purchased
                                            </div>
                                            <div class="history-table-ticketno d-t-c">
                                                Ticket Number
                                            </div>
                                            <div class="history-table-value">
                                                Value
                                            </div>
                                        </div>
                                    </div>
                                    <div class="grid">
                                        <div class="row space-1 grid-item">
                                            <div class="bg-white-bordered col-12">
                                                <!-- <div class="history "> -->
                                                    <div class="history-table py-2">
                                                        <div class="history-table-date">
                                                            <div class="mt-0 hist-titles">
                                                                <div class="">Date:</div>
                                                            </div>
                                                            <div class="history-table-date-title-info">
                                                                <span class="">
                                                                    20/02/2018, 9:00 am
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-user">
                                                            <div class="mt-0 hist-titles">
                                                                <div class="">first name:</div>
                                                            </div>
                                                            <div class="p-info-desc">
                                                                <span class="">
                                                                    Mohd Shahid Afridi
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-purchased">
                                                            <div class="mt-0 hist-titles">
                                                                <div class="">Tickets:</div>
                                                            </div>
                                                            <div class="p-info-desc">
                                                                <span class="color-pink">
                                                                    200
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-ticketno">
                                                            <div class="hist-titles">
                                                                ticket numbers
                                                            </div>

                                                            <div class="p-info-desc secondary-color">
                                                                <span class="color-blue">
                                                                    12,34,54,90,80
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-value">
                                                           <div class="mt-0 hist-titles">
                                                                <div class="">total amount:</div>
                                                            </div>
                                                            <div class="p-info-desc">
                                                                <span class="color-orange">
                                                                    $ 200
                                                                </span>
                                                            </div>
                                                        </div>
                                                    <!-- </div> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid">
                                        <div class="row space-1 grid-item">
                                            <div class="bg-white-bordered col-12">
                                                <!-- <div class="history "> -->
                                                    <div class="history-table py-2">
                                                        <div class="history-table-date">
                                                            <div class="mt-0 hist-titles">
                                                                <div class="">Date:</div>
                                                            </div>
                                                            <div class="history-table-date-title-info">
                                                                <span class="">
                                                                    20/02/2018, 9:00 am
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-user">
                                                            <div class="mt-0 hist-titles">
                                                                <div class="">first name:</div>
                                                            </div>
                                                            <div class="p-info-desc">
                                                                <span class="">
                                                                    Mohd Shahid Afridi
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-purchased">
                                                            <div class="mt-0 hist-titles">
                                                                <div class="">Tickets:</div>
                                                            </div>
                                                            <div class="p-info-desc">
                                                                <span class="color-pink">
                                                                    200
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-ticketno">
                                                            <div class="hist-titles">
                                                                ticket numbers
                                                            </div>

                                                            <div class="p-info-desc secondary-color">
                                                                <span class="color-blue">
                                                                    12,34,54,90,80
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-value">
                                                           <div class="mt-0 hist-titles">
                                                                <div class="">total amount:</div>
                                                            </div>
                                                            <div class="p-info-desc">
                                                                <span class="color-orange">
                                                                    $ 200
                                                                </span>
                                                            </div>
                                                        </div>
                                                    <!-- </div> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid">
                                        <div class="row space-1 grid-item">
                                            <div class="bg-white-bordered col-12">
                                                <!-- <div class="history "> -->
                                                    <div class="history-table py-2">
                                                        <div class="history-table-date">
                                                            <div class="mt-0 hist-titles">
                                                                <div class="">Date:</div>
                                                            </div>
                                                            <div class="history-table-date-title-info">
                                                                <span class="">
                                                                    20/02/2018, 9:00 am
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-user">
                                                            <div class="mt-0 hist-titles">
                                                                <div class="">first name:</div>
                                                            </div>
                                                            <div class="p-info-desc">
                                                                <span class="">
                                                                    Mohd Shahid Afridi
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-purchased">
                                                            <div class="mt-0 hist-titles">
                                                                <div class="">Tickets:</div>
                                                            </div>
                                                            <div class="p-info-desc">
                                                                <span class="color-pink">
                                                                    200
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-ticketno">
                                                            <div class="hist-titles">
                                                                ticket numbers
                                                            </div>

                                                            <div class="p-info-desc secondary-color">
                                                                <span class="color-blue">
                                                                    12,34,54,90,80
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-value">
                                                           <div class="mt-0 hist-titles">
                                                                <div class="">total amount:</div>
                                                            </div>
                                                            <div class="p-info-desc">
                                                                <span class="color-orange">
                                                                    $ 200
                                                                </span>
                                                            </div>
                                                        </div>
                                                    <!-- </div> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid">
                                        <div class="row space-1 grid-item">
                                            <div class="bg-white-bordered col-12">
                                                <!-- <div class="history "> -->
                                                    <div class="history-table py-2">
                                                        <div class="history-table-date">
                                                            <div class="mt-0 hist-titles">
                                                                <div class="">Date:</div>
                                                            </div>
                                                            <div class="history-table-date-title-info">
                                                                <span class="">
                                                                    20/02/2018, 9:00 am
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-user">
                                                            <div class="mt-0 hist-titles">
                                                                <div class="">first name:</div>
                                                            </div>
                                                            <div class="p-info-desc">
                                                                <span class="">
                                                                    Mohd Shahid Afridi
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-purchased">
                                                            <div class="mt-0 hist-titles">
                                                                <div class="">Tickets:</div>
                                                            </div>
                                                            <div class="p-info-desc">
                                                                <span class="color-pink">
                                                                    200
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-ticketno">
                                                            <div class="hist-titles">
                                                                ticket numbers
                                                            </div>

                                                            <div class="p-info-desc secondary-color">
                                                                <span class="color-blue">
                                                                    12,34,54,90,80
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-value">
                                                           <div class="mt-0 hist-titles">
                                                                <div class="">total amount:</div>
                                                            </div>
                                                            <div class="p-info-desc">
                                                                <span class="color-orange">
                                                                    $ 200
                                                                </span>
                                                            </div>
                                                        </div>
                                                    <!-- </div> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid">
                                        <div class="row space-1 grid-item">
                                            <div class="bg-white-bordered col-12">
                                                <!-- <div class="history "> -->
                                                    <div class="history-table py-2">
                                                        <div class="history-table-date">
                                                            <div class="mt-0 hist-titles">
                                                                <div class="">Date:</div>
                                                            </div>
                                                            <div class="history-table-date-title-info">
                                                                <span class="">
                                                                    20/02/2018, 9:00 am
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-user">
                                                            <div class="mt-0 hist-titles">
                                                                <div class="">first name:</div>
                                                            </div>
                                                            <div class="p-info-desc">
                                                                <span class="">
                                                                    Mohd Shahid Afridi
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-purchased">
                                                            <div class="mt-0 hist-titles">
                                                                <div class="">Tickets:</div>
                                                            </div>
                                                            <div class="p-info-desc">
                                                                <span class="color-pink">
                                                                    200
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-ticketno">
                                                            <div class="hist-titles">
                                                                ticket numbers
                                                            </div>

                                                            <div class="p-info-desc secondary-color">
                                                                <span class="color-blue">
                                                                    12,34,54,90,80
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-value">
                                                           <div class="mt-0 hist-titles">
                                                                <div class="">total amount:</div>
                                                            </div>
                                                            <div class="p-info-desc">
                                                                <span class="color-orange">
                                                                    $ 200
                                                                </span>
                                                            </div>
                                                        </div>
                                                    <!-- </div> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid">
                                        <div class="row space-1 grid-item">
                                            <div class="bg-white-bordered col-12">
                                                <!-- <div class="history "> -->
                                                    <div class="history-table py-2">
                                                        <div class="history-table-date">
                                                            <div class="mt-0 hist-titles">
                                                                <div class="">Date:</div>
                                                            </div>
                                                            <div class="history-table-date-title-info">
                                                                <span class="">
                                                                    20/02/2018, 9:00 am
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-user">
                                                            <div class="mt-0 hist-titles">
                                                                <div class="">first name:</div>
                                                            </div>
                                                            <div class="p-info-desc">
                                                                <span class="">
                                                                    Mohd Shahid Afridi
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-purchased">
                                                            <div class="mt-0 hist-titles">
                                                                <div class="">Tickets:</div>
                                                            </div>
                                                            <div class="p-info-desc">
                                                                <span class="color-pink">
                                                                    200
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-ticketno">
                                                            <div class="hist-titles">
                                                                ticket numbers
                                                            </div>

                                                            <div class="p-info-desc secondary-color">
                                                                <span class="color-blue">
                                                                    12,34,54,90,80
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-value">
                                                           <div class="mt-0 hist-titles">
                                                                <div class="">total amount:</div>
                                                            </div>
                                                            <div class="p-info-desc">
                                                                <span class="color-orange">
                                                                    $ 200
                                                                </span>
                                                            </div>
                                                        </div>
                                                    <!-- </div> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid">
                                        <div class="row space-1 grid-item">
                                            <div class="bg-white-bordered col-12">
                                                <!-- <div class="history "> -->
                                                    <div class="history-table py-2">
                                                        <div class="history-table-date">
                                                            <div class="mt-0 hist-titles">
                                                                <div class="">Date:</div>
                                                            </div>
                                                            <div class="history-table-date-title-info">
                                                                <span class="">
                                                                    20/02/2018, 9:00 am
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-user">
                                                            <div class="mt-0 hist-titles">
                                                                <div class="">first name:</div>
                                                            </div>
                                                            <div class="p-info-desc">
                                                                <span class="">
                                                                    Mohd Shahid Afridi
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-purchased">
                                                            <div class="mt-0 hist-titles">
                                                                <div class="">Tickets:</div>
                                                            </div>
                                                            <div class="p-info-desc">
                                                                <span class="color-pink">
                                                                    200
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-ticketno">
                                                            <div class="hist-titles">
                                                                ticket numbers
                                                            </div>

                                                            <div class="p-info-desc secondary-color">
                                                                <span class="color-blue">
                                                                    12,34,54,90,80
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="history-table-value">
                                                           <div class="mt-0 hist-titles">
                                                                <div class="">total amount:</div>
                                                            </div>
                                                            <div class="p-info-desc">
                                                                <span class="color-orange">
                                                                    $ 200
                                                                </span>
                                                            </div>
                                                        </div>
                                                    <!-- </div> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            
                        </div>
                    </div>
                </div>
                
            </div>
        </section>
    </div>
    <div class="modal fade modal-mini modal-danger" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header justify-content-center">
                    <div class="modal-profile">
                        <i class="ti-close"></i>
                    </div>
                </div>
                <div class="modal-body">
                    <h5 class="color-white text-center">Delete for Sure?</h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link btn-neutral" data-dismiss="modal">No</button>
                    <button type="button" class="btn btn-link btn-neutral" data-dismiss="modal">Yes</button>
                </div>
            </div>
        </div>
    </div>
    
    <?php
    include("_inc/footer.php");
    ?>
    
    <script type="text/javascript">
        $(".delete-btn").on('click', function(){
            $(this).closest('.grid-item').remove();
        });
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script> 

    <script src="./assets/js/new_luxdraws.js" type="text/javascript"></script>