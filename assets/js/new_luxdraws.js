$(document).ready(function(){

    $.widget("ui.autocomplete", $.ui.autocomplete, {
        options: {
            animation: ''
        },
        _suggest: function(items) {
            var self = this;
            var animationName = self.options.animation;
            if ( animationName === '' ) {
                self._super(items);
                return;
            }
            if ( self.menu.element.hasClass('animated') ) {
                if ( self.menu.element.data('animation_ended') ) {
                    self._super(items);
                }
                else {
                    var _super = self._super.bind(self, items);
                    self.menu.element.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',function(){
                    $(this).data('animation_ended', true);
                    _super();
                    });
                }
            }
            else {
                self._super(items);
                self.menu.element
                .data('animation_ended', false)
                .addClass('animated ' + animationName)
                .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                    $(this).data('animation_ended', true);
                });
            }
        },
    
        _close: function(event) {
            var self = this;
            self._super(event);
            if ( self.options.animation ) {
                self.menu.element.removeClass('animated ' + self.options.animation);
            }
        },
    });
    (window.jQuery);


    var availableTags = [
        "Rolex",
        "Tag Heuer",
        "Patek Philippe",
        "Vacheron Constantin",
        "Audemars Piguet"
    ];

    $( ".slide input" ).each(function(){
        $(this).autocomplete({
            source: availableTags,
            delay: 100,
            animation: $(this).prev().text()
        });
    });


    $('[data-search]').on('keyup', function() {
        var searchVal = $(this).val();
        var filterItems = $('[data-filter-item]');

        if ( searchVal != '' ) {
            filterItems.addClass('d-none');
            $('[data-filter-item][data-filter-name*="' + searchVal.toLowerCase() + '"]').removeClass('d-none');
        } else {
            filterItems.removeClass('d-none');
        }
    });

});

