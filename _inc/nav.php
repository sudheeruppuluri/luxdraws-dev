<!--     <script src="./assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
    <script src="./assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="./assets/js/core/bootstrap.min.js" type="text/javascript"></script> -->
    <!-- <script src="./assets/js/plugins/bootstrap-switch.js"></script> -->
    <!-- <script src="./assets/js/plugins/nouislider.min.js" type="text/javascript"></script> -->

    <!-- <script src="./assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script> -->

    <!-- <script src="./assets/js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script> -->
    
    <!-- <script src="./assets/js/lightbox-plus-jquery.min.js"></script> -->
    <!-- <script src="https://npmcdn.com/isotope-layout@3.0.5/dist/isotope.pkgd.js"></script>  -->

    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>  -->

    <!-- <script src="./assets/js/new_luxdraws.js?v=1.1.0" type="text/javascript"></script> -->
     

    

 <!-- Navbar -->
        <nav class="navbar navbar-expand-lg fixed-top navbar-luxdraws " color-on-scroll="400">
            <div class="container-fluid">
                <div class="navbar-translate">
                    <a class="navbar-brand" href="" rel="tooltip" title="" data-placement="bottom" target="_blank" style="font-size: 20px;">
                        <!-- <img src="./assets/img/logo.png"> -->
                        LUXDRAWS
                    </a>
                    <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
            </div>
        </nav>
        <nav class="navbar navbar-expand-lg fixed-top bg-white-sm bg-white navbar-secondary bg-white-to-trans" style="margin-top: 53px;">
            <div class="container">
                <div class="collapse navbar-collapse flex-container" id="navigation" data-nav-image="./assets/img/blurred-image-1.jpg">
                    <ul class="navbar-nav">
                        <li class="nav-item <?php if($current == 'dashboard') {echo 'active';} ?>">
                            <a class="nav-link" href="index.php">
                                <i class="nav-icon nav-icon-dashboard"></i>
                                Dashboard
                            </a>
                        </li>
                        <li class="nav-item <?php if($current == 'add-product') {echo 'active';} ?>">
                            <a class="nav-link" href="addproduct.php">
                                <i class="nav-icon nav-icon-product"></i>
                                Add Products
                            </a>
                        </li>
                        <li class="nav-item <?php if($current == 'product-list') {echo 'active';} ?>">
                            <a class="nav-link" href="product-list.php">
                                <i class="nav-icon nav-icon-product-list"></i>
                                Products List
                            </a>
                        </li>
                        <li class="nav-item <?php if($current == 'product-history') {echo 'active';} ?>"">
                            <a class="nav-link" href="product-history.php">
                                <i class="nav-icon nav-icon-expired-product"></i>
                                Product history
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>