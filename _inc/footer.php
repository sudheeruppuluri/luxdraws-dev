    <footer class="footer footer-default" style="position: relative; bottom: 0;">
        <div class="container">
            
            <div class="copyright">
                &copy;
                <script>
                document.write(new Date().getFullYear())
                </script>
                <a href="/" target="_blank">Luxdraws</a>.
            </div>
        </div>
    </footer>
    </body>
    <!--   Core JS Files   -->

    <script src="./assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>

    <script src="./assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="./assets/js/core/bootstrap.min.js" type="text/javascript"></script>
    <!-- <script src="./assets/js/plugins/bootstrap-switch.js"></script> -->
    <!-- <script src="./assets/js/plugins/nouislider.min.js" type="text/javascript"></script> -->
    <script src="./assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
    
    <script src="./assets/js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script>
    <!-- <script src="./assets/js/lightbox-plus-jquery.min.js"></script> -->
    <!-- <script src="https://npmcdn.com/isotope-layout@3.0.5/dist/isotope.pkgd.js"></script>  -->
    

</html>