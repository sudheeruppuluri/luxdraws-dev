<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="./assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>LUXDRAWS</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
        <!-- CSS Files -->
        <link href="./assets/css/bootstrap.min.css" rel="stylesheet" />
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
        <link href="./assets/css/style.css?v=1.1.0" rel="stylesheet" />

         <link href="./assets/css/sudheer-global.css?v=1.1.0" rel="stylesheet" />

         <link href="./assets/css/add_product.css?v=1.1.0" rel="stylesheet" />

         <link href="./assets/css/themify-icons.css?v=1.1.0" rel="stylesheet" />

         <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.4/cropper.min.css'>
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <!-- <link href="./assets/css/demo.css" rel="stylesheet" /> -->
    </head>
    <body class="landing-page sidebar-collapse">
        <?php
            $current = 'edit-product'
        ?>
        <?php
        include("_inc/nav.php");
        ?>
        <!-- End Navbar -->
        <div class="wrapper" style="padding-top: 150px;">

            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-12 title-dotted-style space-1">
                            <h4>Edit Product</h4>
                        </div>
                    </div>

                     
                    <div class="bg-white-bordered global-padding-more space-2">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row space-2 mt-4">
                                    <label for="productname" class="col-12 col-sm-4 col-xl-2 col-form-label text-sm-right">
                                        Product Name
                                    </label>
                                    <div class="col-12 col-sm-6 col-xl-4">
                                        <input type="name" class="form-control" id="productname" placeholder="Enter Product Name">
                                    </div>
                                </div>

                                <div class="form-group row space-2">
                                    <label for="modelNumber" class="col-12 col-sm-4 col-xl-2 col-form-label text-sm-right">
                                        Model Number
                                    </label>
                                    <div class="col-12 col-sm-6 col-xl-4">
                                        <input type="name" class="form-control" id="modelNumber" placeholder="Enter Model No">
                                    </div>
                                </div>

                                <div class="form-group row space-1">
                                    <label for="productImage" class="col-12 col-sm-4 col-xl-2 col-form-label text-sm-right">
                                        Product Image
                                    </label>
                                    <div class="col-12 col-sm-6 col-xl-4 space-1">
                                        <input type="name" class="form-control" id="productImage" placeholder="Uplaod Image">
                                    </div>
                                    <div class="col-12 col-sm-2 col-xl-2">
                                        <div class="file-upload">
                                             
                                            <input type="file" id="file-input" class="" style="opacity: 0; position: absolute;">
                                            <label for="file-input" id="file-input" class="btn btn-primary m-auto">
                                                Upload
                                            </label>
                                        </div>
                                    </div>
                                    <div class="box-2 result-box offset-lg-2">
                                        <div class="result"></div>
                                    </div>
                                    <!--rightbox-->
                                    <div class="box-2 img-result hide">
                                        <!-- result of crop -->
                                        <img class="cropped" src="" alt="">
                                    </div>
                                    <!-- input file -->
                                    <div class="box" style="display: none;">
                                        <div class="options hide">
                                            <label> Width</label>
                                            <input type="number" class="img-w" value="300" min="100" max="1200" />
                                        </div>
                                        <!-- save btn -->
                                        
                                    </div>
                                    <div class="w-100">
                                        <a class="btn save hide save-img">Save</a>
                                    </div>
                                </div>

                                <div class="form-group row space-2">
                                    <label for="startDate" class="col-12 col-sm-4 col-xl-2 col-form-label text-sm-right">
                                        Start Date
                                    </label>
                                    <div class="col-12 col-sm-6 col-xl-4">
                                        <input type="text" class="form-control date-picker" value="10/04/2018"/ data-datepicker-color="" id="startDate">
                                    </div>
                                </div>

                                <div class="form-group row space-2">
                                    <label for="endDate" class="col-12 col-sm-4 col-xl-2 col-form-label text-sm-right">
                                        End Date
                                    </label>
                                    <div class="col-12 col-sm-6 col-xl-4">
                                        <input type="text" class="form-control date-picker" value="10/05/2018"/ data-datepicker-color="" id="endDate">
                                    </div>
                                </div>

                                <div class="form-group row space-2">
                                    <label for="productPrice" class="col-12 col-sm-4 col-xl-2 col-form-label text-sm-right">
                                        Product Price
                                    </label>
                                    <div class="col-12 col-sm-6 col-xl-4">
                                        <input type="name" class="form-control" id="productPrice" placeholder="Price">
                                    </div>
                                </div>

                                <div class="form-group row space-5">
                                    <label for="totalTickets" class="col-12 col-sm-4 col-xl-2 col-form-label text-sm-right">
                                       Total Tickets
                                    </label>
                                    <div class="col-12 col-sm-6 col-xl-4">
                                        <input type="name" class="form-control" id="totalTickets" placeholder="Total Tickets">
                                    </div>
                                </div>

                                <div class="form-group row space-2 mr-2">
                                    <button type="button" class="btn btn-lg btn-save" style="margin-left: 10%;">
                                        Save
                                    </button>
                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <?php
        include("_inc/footer.php");
        ?>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.js'></script>
        <script src='./assets/js/cropper.js'></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $(".save-img").on('click', function(){
                    $(".result-box").hide();
                    $(".save-img").hide();
                    $(".img-result").show();
                });
            });
        </script>


        