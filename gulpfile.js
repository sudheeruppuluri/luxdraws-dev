
var gulp = require('gulp');

var sass = require('gulp-sass');

var browserSync = require('browser-sync').create();

//browserSync Task

gulp.task('browserSync', function() {
	browserSync.init({
	proxy: 'http://localhost/luxdraws-dev',
	host: '',
	open: 'external'
  })
})

//SCSS compiling Task

gulp.task('sass', function(){
	return gulp.src('assets/scss/**/*.scss')	 // Gets all files ending with .scss in dev/scss
	.pipe(sass())							// Converts Sass to CSS with gulp-sass
	.pipe(gulp.dest('assets/css'))
	.pipe(browserSync.reload({
      stream: true
    }))
});

//Watching all tasks

gulp.task('watch', ['browserSync', 'sass'], function (){
  gulp.watch('assets/scss/**/*.scss', ['sass']);
  gulp.watch('/*.php', browserSync.reload); 
})